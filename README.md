# VTKHDF-scripts

Repository used to store h5py scripts to generate VTKHDF file format based on the [file format specification](https://docs.vtk.org/en/latest/design_documents/VTKFileFormats.html#hdf-file-formats).

## scripts

`scripts` folder contains h5py scripts to generate VTKHDF File format. For now we can generate:

- PolyData
- UnstructuredGrid
- ImageData
- Composite

Note that to generate temporal data it requires `vtkpython`.

## vtkxml-to-vtkhdf

This folder contains a script named vtkxml-to-vtkhdf.py which converts VTK XML image data and unstructured grid, serial and parallel formats to the VTK HDF format.

We include a simple test script called `test.sh` that converts a file stored using each of the supported input formats into VTK HDF and then it checks the results. The script should not produce any difference with the stored baselines.

To be able to use this `test.sh`, it will be needed to clone the sample data repository : https://gitlab.kitware.com/keu-public/vtkhdf/vtkhdf-data-samples. An editing theses 2 variables:
- `VTK_PYTHONPATH` : for where your vtkpython is located
- `VTKHDF_DATA_SAMPLES_LOCATION` : for where the data samples repository has been cloned.

Note that to be able to use this `test.sh`, you will need to clone this repository which have

## License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
